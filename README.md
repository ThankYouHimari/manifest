# Paranoid Android #

```bash
# Install Repo in the created directory
$ repo init --depth=1 -u https://gitlab.com/ThankYouHimari/manifest -b uvite
```

### Downloading the source tree ###

This is what you will run each time you want to pull in upstream changes. Keep in mind that on your
first run, it is expected to take a while as it will download all the required Android source files
and their change histories.

```bash
# Let Repo take care of all the hard work
#
# The -j# option specifies the number of concurrent download threads to run.
# 4 threads is a good number for most internet connections.
# You may need to adjust this value if you have a particularly slow connection.
$ repo sync --current-branch --no-tags -j$(nproc --all)
```
